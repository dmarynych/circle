import requests
import argparse
from subprocess import check_output

parser = argparse.ArgumentParser(description='Send slack notifications.')
parser.add_argument('--channel', default='#vgs_sysops',
                    help='Channel to send notification')
parser.add_argument('--env', default='dev',
                    help='Environment for deploy')
args = parser.parse_args()

def getRevision():
    rev = check_output(['git', 'log', '--pretty=oneline', '-1'])
    return rev.decode("utf-8")

def getUser():
    try:
        output = check_output(['git', 'config', 'user.name']).decode("utf-8")
    except Exception:
        output = "circleci"
    return output

def sendNotification(env, channel, revision, user):
    slackUrl = 'https://hooks.slack.com/services/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'
    message = "Redeploying " + env + "\n"+ revision + "\n by " + user
    data = {
        "channel": "#"+ channel,
        "attachments": [{"color": "#C0C0C0", "text": message, "mrkdwn_in": ["text", "pretext"]}]
    }

    requests.post(slackUrl, headers={'Content-Type': 'application/json'}, json=data)


sendNotification(args.env, args.channel, getRevision(), getUser())
